// rafce
import React from 'react'
import { detailPokemon, Pokemon } from '../interFace'
import PokemonList from './PokemonList';
import './Pokemon.css'
import { ViewDetail } from '../App';

// Tạo type cho cái props đc nhận
interface Props{
  pokemons: detailPokemon[];
  viewDetail: ViewDetail;

  // hover lên setViewDetail rồi copy vào
  setViewDetail:React.Dispatch<React.SetStateAction<ViewDetail>>
}



const PokemonCollestion:React.FC<Props> = (props) => {
  
  const {pokemons , viewDetail , setViewDetail} = props
  console.log('viewDetail: ', viewDetail);
  //  Viết hàm khi click vào thì lấy ra được id của các con pokemon
  
  const selectPokemon = (id: number) => {
    if(!viewDetail.isOpen) {
      setViewDetail({
      // Ở trong view Detail
      id : id,
      isOpen: true
    }
    )
    }
  }

  return (
    
    <>
      <section className={viewDetail.isOpen ? 'collection-container-active' : 'collection-container'}>
        {viewDetail.isOpen ? (
          <div className="overlay"></div>
        ) : (
          <div className=''></div>
        )}
        {pokemons.map((poke) => {
          return (
            <div onClick={() => selectPokemon(poke.id)}>
              <PokemonList
              key={poke.id}
              name={poke.name}
              id={poke.id}
              img={poke.sprites.front_default}
              viewDetail={viewDetail}
              setViewDetail={setViewDetail}
              // Hiện ra detail khi click vào 
              abilities = {poke.abilities}
              />
            </div>
          )
        })}
      </section>
    </>
  )
}

export default PokemonCollestion
