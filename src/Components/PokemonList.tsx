import React, { useEffect, useState } from 'react'
import { ViewDetail } from '../App';
import './Pokemon.css'

// Tạo type cho cái props đc nhận
interface Props{
    name: string;
    id: number;
    img: string;
    abilities: {
        ability: string;
        name: string;
        // [] cho nó là 1 array

        // phải | undefined vì khi mình fetch api về thì nó có thể bị underfined nên typeScript muốn đảm bảo 
        // mình không lấy 1 cái data mà nó không có thật 
    }[] | undefined;

    viewDetail: ViewDetail;
  // hover lên setViewDetail rồi copy vào
  setViewDetail:React.Dispatch<React.SetStateAction<ViewDetail>>
  }

  const PokemonList:React.FC<Props> = (props) => {
    const {name , id ,img , abilities , viewDetail , setViewDetail} = props

 const [isSelected , setIsSelected] = useState(false)

useEffect(()=>{
setIsSelected(id === viewDetail?.id)
},[viewDetail])

const closeDetail = () => {
    setViewDetail({
      id: 0,
      isOpen: false,
    });
  };

return (
    <div className="">
      {isSelected ? (
        <section className="pokemon-list-detailed">
          <div className="detail-container">
            <p className="detail-close" onClick={closeDetail}>
              X
            </p>
            <div className="detail-info">
              <img src={img} alt="pokemon" className="detail-img" />
              <p className="detail-name"> {name}</p>
            </div>
            <div className="detail-skill">
              <p className="detail-ability"> Ablities: </p>
              {abilities?.map((ab: any) => {
                return <div className=""> {ab.ability.name}</div>;
              })}
            </div>
          </div>
        </section>
      ) : (
        <section className="pokemon-list-container">
          <p className="pokemon-name"> {name} </p>
          <img src={img} alt="pokemon" />
        </section>
      )}
    </div>
  );
}

export default PokemonList