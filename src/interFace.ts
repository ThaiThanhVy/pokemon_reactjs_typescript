export interface Pokemon{
    id: number,
    name:string,
    // Chứa hình ảnh của các con pokemon
    sprites: {
      front_default: string
    }
  
  }
  
// Tạo interface mới kế thừa từ interface Pokemon

export interface detailPokemon extends Pokemon{

    // Dấu ? là có cx đc không có cx được
    abilities?: {
        ability: string;
        name: string;
    
        // [] Cho nó là 1 array
    }[]
}