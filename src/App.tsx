import axios from 'axios';
import React, { useEffect, useState } from 'react';
import './App.css';
import PokemonCollestion from './Components/PokemonCollestion';
import { Pokemon } from './interFace';


// Định nghĩa interFace cho name và url mà api trả về

interface Pokemons{
  name:string,
  url: string
}

export interface ViewDetail {
  id: number,
  isOpen: boolean,
}
// Nói cho react biết đây là function component
const App:React.FC = () => {

  // useState này là 1 array và kiểu dữ liệu là string
  const [pokemons, setPokemos] = useState<Pokemon[]>([]) 
  const [nextUrls, setNextUrls] = useState<string>("") 
  const [loading, setLoading] = useState<Boolean>(true) 
  const [viewDetail, setViewDetail] = useState<ViewDetail>({
    id: 0,
    isOpen: false,
  }) 
  
  // Sử dụng useEffect để lấy api về nó sẽ chạy lần đầu đi load trang

  useEffect(() => {
    const getPokemon = async() => {
      const res = await axios.get('https://pokeapi.co/api/v2/pokemon?limit20&offset20')

      // res.data.next là khi bấm vào nó sẽ in ra 20 con nữa
      console.log('res.data.next: ', res.data.next);
      setNextUrls(res.data.next)

      // Chậy vòng lập để lấy ra đc thông tin của từng con
      res.data.results.forEach(async(pokemon:Pokemons) => {
        const poke = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`)
        console.log('poke: ', poke.data);

        // Api sẽ trả về 1 { } nên không thể dùng hàm map để . tới đc nên sử dụng setPokemons để biến nó thành []
        setPokemos((p) => [...p , poke.data])
        setLoading(false)
      })

    }

    getPokemon()
  },[])

  const nextPage = async() => {
    setLoading(true)
    const res = await axios.get(nextUrls)

    setNextUrls(res.data.next)

    res.data.results.forEach(async(pokemon:Pokemons) => {
      const poke = await axios.get(`https://pokeapi.co/api/v2/pokemon/${pokemon.name}`)
      console.log('poke: ', poke.data);

      // Api sẽ trả về 1 { } nên không thể dùng hàm map để . tới đc nên sử dụng setPokemons để biến nó thành []
      setPokemos((p) => [...p , poke.data])
      setLoading(false)
    })
  }
  

  return (
    <div className="App">
     <div className="container">
      <header className='pokemon-header'>Pokemon</header>

      {/* Chuyền props */}
      {/* Lưu ý bên nhận phải khai báo biến type cho cái props nhận đc nếu không nó sẽ bị lỗi */}
      <PokemonCollestion  pokemons={pokemons} viewDetail={viewDetail} setViewDetail={setViewDetail}/>
      <div className='btn'>
        {!viewDetail.isOpen ? <button onClick={nextPage}>{loading ? 'Loading ...' : 'Load more'}</button> : ''}
       
      </div>
     </div>
    </div>
  );
}

export default App;
